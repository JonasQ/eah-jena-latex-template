
echo "To run this script, detex is required! "

rm wordcounts.txt 2> /dev/null

detex main.tex > tmp_thesis_main.txt

# Removes everything before Einleitung and after Anhang
BEGINNING="Zielstellung"
ENDING="Abbildungsverzeichnis"
while read p; do
  # end txt when end was found
  echo "$p" | grep $ENDING && break
  if [ "$FOUNDBEGIN" == "y" ]
      then
          echo "$p" >>wordcounts.txt
          # echo "$p" | sed 's/\[[\w. ]*\][[\w. ]*\]\w+\.\w+/ /g' >>wordcounts.txt
  fi
  # start txt when beginning was found
  echo "$p" | grep $BEGINNING >/dev/null && FOUNDBEGIN="y"
done <tmp_thesis_main.txt

echo
echo "Anzahl der Wörter vom Kapitel $BEGINNING bis $ENDING:"
wc -w wordcounts.txt # zeilen, wörter, zeichen
rm wordcounts.txt 2> /dev/null
rm tmp_thesis_main.txt 2> /dev/null
