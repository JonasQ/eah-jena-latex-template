# EAH Jena LaTex Template

In this repository I would like to share the template I created for theses, term papers and other scientific work with LaTex, because such a template is not provided by EAH Jena. 


Currently, the document consists of my cannibalized bachelor thesis, which is why the template is far from perfect. The document is currently also in German, but I would like to add the possibility to switch the language and additionally divide the template into final papers and normal seminar papers. I hope it still helps someone.

